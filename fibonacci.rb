class Fibonacci
  def self.fibonacci_digit_counter length:
    num1 , num2 , i = -1, 0, 1
    while i.to_s.length < length
      num1 +=1
      i,num2 = num2, num2+i
    end
    puts num1
  end
end
Fibonacci.fibonacci_digit_counter length: 2000