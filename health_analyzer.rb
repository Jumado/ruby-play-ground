require 'rubygems'
require 'decisiontree'
# Analyzes health based on temperature
class HealthAnalyzer
  def initialize
    attributes = ['Temp']
    train = [[98.7, 'Healthy'], [99.1, 'Healthy'], [99.5, 'Sick']]
    train += [[102.5, 'Crazy Sick'], [107.5, 'Dead']]
    @dec_tree = DecisionTree::ID3Tree
    @dec_tree = @dec_tree.new(attributes, train, 'Sick', :continuous)
    @dec_tree.train
  end

  def test(temp)
    @dec_tree.predict(temp)
  end
end
result = HealthAnalyzer.new
p result.test([67, 'Healthy'])
