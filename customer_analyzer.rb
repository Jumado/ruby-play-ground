require 'rubygems'
require 'decisiontree'
# Analyzes  client if can custome
class CustomerAnalyzer
  def initialize
    attributes = ['Age', 'Education', 'Income', 'Marital Status']
    training = [
      ['36-55', 'Masters', 'High', 'Single', 1],
      ['18-35', 'High School', 'Low', 'Single', 0],
      ['36-35', 'Masters', 'High', 'Single', 1],
      ['18-35', 'PHD', 'Low', 'Married', 1],
      ['<18', 'High School', 'Low', 'Single', 1],
      ['55+', 'High School', 'High', 'Married', 0],
      ['55+', 'High School', 'High', 'Married', 1],
      ['55+', 'High School', 'High', 'Married', 1],
      ['55+', 'High School', 'High', 'Married', 1],
      ['<18', 'Masters', 'Low', 'Single', 0]
    ]
    @doc_tree = DecisionTree::ID3Tree.new(attributes, training, 1, :discrete)
    @doc_tree.train
  end

  def predict(customer_info)
    decision = @doc_tree.predict(customer_info)
    decision == 1 ? 'Potential  customer' : 'Don\'t bother'
  end
end
test = ['18-35', 'High School', 'Low', 'Married']
p CustomerAnalyzer.new.predict(test)
test2 = ['36-55', 'Bachelors', 'High', 'Single', 1]
p CustomerAnalyzer.new.predict(test2)
