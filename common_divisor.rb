# Common divsior
# Reduce example
class CommonDivisor
  def self.least_comm_divisor(*numbers)
    numbers.reduce(:lcm)
  end

  def self.greatest_common_divisor(*numbers)
    numbers.reduce(:gcd)
  end
end
p CommonDivisor.least_comm_divisor(32, 56, 8)
p CommonDivisor.greatest_common_divisor(32, 56, 8)
