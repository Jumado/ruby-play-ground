class Permutation
  def self.nth_lexicographic_permutation(range_start: , range_end:,position:)
    (range_start..range_end).to_a.permutation.to_a[position].join
  end
end
puts Permutation.nth_lexicographic_permutation range_start: 0, range_end: 9,position: 999_999