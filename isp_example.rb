require 'forwardable'
#  Blog class
class Blog
  def edit_post
    puts 'Post edited'
  end

  def delete_post
    puts 'Post deleted'
  end
end
require 'forwardable'
# Can only edit posts
class Moderator
  extend Forwardable
  def_delegators:@blog, :edit_post
  def initialize(blog)
    @blog = blog
  end
end
moderator = Moderator.new(Blog.new)
moderator.edit_post
