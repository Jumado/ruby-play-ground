require 'mathn'
#  Take number identify all its divisors
# add them together to get a value
#  take the final sum value
# identify it's divisors and add them together
#  if the second sum value is the same as original number
#  for which you found the divisors
# both the original number and its final sum
# are amicable numbers
class AmicableNumber
  def self. dsum(number)
    return 1 if number < 2
    pd = number.prime_division.flat_map { |n, p| [n] * p }
    (([1] + (1...pd.size).to_a)
    .flat_map { |e| pd.combination(e).map { |f| f.reduce(:*) } })
      .uniq.inject(:+)
  end

  def self.find_d_sum(n)
    n.times.inject  do |sum, cur|
      other = dsum(cur)
      cur != other && dsum(other) == cur ? sum + cur : sum
    end
  end
end
p AmicableNumber.find_d_sum(9_999)
