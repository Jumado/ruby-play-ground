class Factorial
  def self.factorial_value_sum_generator(factorial)
    (1..factorial).to_a.reverse.each { |i| factorial += factorial * (i-1) }
    factorial.to_s.split(//).map(&:to_i).inject(:+)
  end
  def self.generate_factorial_digits(number)
    (1..number).to_a.reverse.each { |i| number += number * (i-1) }.join('x')
  end
end
p Factorial.factorial_value_sum_generator(3)
p Factorial.generate_factorial_digits(3)