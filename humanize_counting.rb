require 'humanize'
# Counts numbers
class HumanizeCounting
  def self.count_numbers(from:, to:)
    (from..to).to_a.map(&:humanize).join(',').tr('-', ' ')
  end
end
p HumanizeCounting.count_numbers from: 1, to: 1_000_000
