require 'date'
# Number of Sundays
class DateAlgorithm
 def self.number_of_sundays(start_date: , end_date:)
 start_date = Time.local(start_date)
 end_date = Time.local(end_date)
  sunday_counter = 0
  while end_date >= start_date
    if end_date.strftime('%A') =='Sunday' &&
      end_date.strftime('%d') == '01'
      sunday_counter+=1
    end
   end_date -= 86400
  end
  puts sunday_counter
 end
end

DateAlgorithm.number_of_sundays(start_date: 1901, end_date: '2000/12/31')
