require 'ostruct'
# Method missing demo
class Author
  attr_accessor :first_name, :last_name, :genre
  genres  = %w(fiction coding history)
  def author
    OpenStruct.new(first_name: first_name, last_name: last_name, genre: genre)
  end

  def method_missing(method_name, *arguments, &block)
    if method_name.to_s =~ /author_(.*)/
      author.send($1, *arguments, &block)
    else
      super
    end
  end

  def respond_to_missing?(method_name, include_private = false)
    method_name.to_s.start_with?('author_') || super
  end
  genres.each do |genre|
    define_method("#{genre}_details") do |arg|
      puts "Genre: #{genre}"
      puts arg
      puts genre.object_id
    end
  end
end
author = Author.new
author.first_name = 'Cal'
author.last_name =  'NewPot'
author.genre = 'Computer science'
puts author.author_genre
puts author.author_first_name
puts author.inspect
puts author.respond_to?(:author_genre)
author.coding_details('Cal new Report')
p author.respond_to?(:history_details)
