# Sorting algorithm class
class SortAlgorithm
  def self.bubble_sort(array)
    n = array.length
    loop do
      swapped = false
      (n - 1).times do |i|
        swapped = swap(array, i)
      end
      break unless swapped
    end
    array
  end

  def self.quick_sort(array)
    return array if array.empty?
    pivot = array.delete_at rand array.size
    left, right = array.partition(&pivot.method(:>))
    [*quick_sort(left), pivot, *quick_sort(right)]
  end

  def self.merge_sort(list)
    return list if list.length == 1
    mid = (list.length / 2).floor
    left = merge_sort(list[0..mid - 1])
    right = merge_sort(list[mid..list.length])
    merge_list(left: left, right: right)
  end

  def self.swap(array, i)
    return unless array[i] > array[i + 1]
    array[i], array[i + 1] = array[i + 1], array[i]
    true
  end

  def self.merge_list(left:, right:)
    return right if left.empty?
    return left if  right.empty?
    merge_array left: left, right: right
  end

  def self. merge_array(left:, right:)
    if left.first < right.first
      [left.first] + merge_list(left: left[1..left.length], right: right)
    else
      [right.first] + merge_list(left: left, right: right[1..right.length])
    end
  end
end
a = [1,4,1,3,4,1,3,3]
p SortAlgorithm.merge_sort(a)
