require 'prime'
# Prime numbers generator
class PrimeNumbers
  def self.generate_prime_numbers(max:)
    prime_array = Prime.take_while{|p| p < max}
    prime_array.each{|p| puts p}
    puts " Sum is #{ prime_array.inject(&:+)}"
  end
end
PrimeNumbers.generate_prime_numbers(max: 2_000_000)