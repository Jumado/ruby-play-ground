class String
  def censor word
    self.gsub!("#{word}",'CENSORED')
  end
end